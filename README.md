git clone https://gitlab.cern.ch/skandel/pmtabsolutegain.git

cd pmtabsolutegain

source setup.sh

#Example Usage

#By default -o is ../results/Data/ for all scripts

python DatToCsv.py -i ../

python CSVToCoefficients.py

python Gain.py

python merge_gains.py
