#!/bin/bash

if [ ! -d "pmtabsgain" ]; then
    python3 -m venv pmtabsgain
    source pmtabsgain/bin/activate
else
    echo "Virtual environment already exists. Activating..."
    source pmtabsgain/bin/activate
fi

if [ -f requirements.txt ]; then
    pip install -r requirements.txt
else
    echo "No requirements.txt found. Skipping dependency installation."
fi

chmod +x scripts/*py
echo "Setup Completed!"
