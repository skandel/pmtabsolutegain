#!/usr/bin/env python3

import os
import pandas as pd
import logging
import numpy as np
from datetime import datetime
import argparse

def process_csv(file_path, output_directory, cycle_flag, fw_remove=[]):
    try:
        data = pd.read_csv(file_path)
        date = os.path.basename(file_path).split('-')[1]
        #Don't change columns if no FW are to be removed
        if not fw_remove:
            required_columns_x = [f'Cycle{i}_{j}_ave_sig' for i in range(1, 11) for j in range(1, 7)]
            required_columns_y = [f'Cycle{i}_{j}_sig_sig' for i in range(1, 11) for j in range(1, 7)]
            
            each_cycle_cols_x = [required_columns_x[i:i + 6] for i in range(0, len(required_columns_x), 6)]
            each_cycle_cols_y = [required_columns_y[i:i + 6] for i in range(0, len(required_columns_y), 6)]
        # For each FW position specified to be removed, drop relevant columns
        else:

            for fw in fw_remove:
                cols_to_remove = [f'Cycle{i}_{fw}_{label}' for i in range(1, 11) for label in ['ave_ped', 'sig_ped', 'ave_raw', 'sig_raw', 'ave_sig', 'sig_sig']]
                data.drop(columns=cols_to_remove, inplace=True, errors='ignore')

            required_columns_x = [f'Cycle{i}_{j}_ave_sig' for i in range(1, 11) for j in range(1, 7) if j not in fw_remove]
            required_columns_y = [f'Cycle{i}_{j}_sig_sig' for i in range(1, 11) for j in range(1, 7) if j not in fw_remove]

            each_cycle_cols_x = [required_columns_x[i:i + 6-len(fw_remove)] for i in range(0, len(required_columns_x), 6-len(fw_remove))]
            each_cycle_cols_y = [required_columns_y[i:i + 6-len(fw_remove)] for i in range(0, len(required_columns_y), 6-len(fw_remove))]

        if cycle_flag:
            cycle_string = 'CycleWise'
            columns = {
            'Ch': [],
            **{f'Cycle{cycle}_{param}': [] for cycle in range(1, 11) for param in 'ABC'},
            **{f'Cycle{cycle}_{param}_err': [] for cycle in range(1, 11) for param in 'ABC'}
            }
            if len(each_cycle_cols_x[0])<3:
                raise ValueError("Are you alright? You need n+1 points for n-th order polynomial fit. INCLUDE MORE FILTERS. For quadratic fit you need at least 3 filter wheel positions")
            for index, row in data.iterrows():
                params = []
                param_errs = []
                for cycle_index, cycle_fw_name in enumerate(each_cycle_cols_x):
                    
                    x = row[cycle_fw_name].values
                    y = row[each_cycle_cols_y[cycle_index]].values ** 2
                    coef, error = np.polyfit(x,y,2, cov=True)
                    fit_error = np.sqrt(np.diag(error))
                    params.append(coef[::-1])
                    param_errs.append(fit_error[::-1])
                
                flat_params_errs = list(np.array(params).flatten()) + list(np.array(param_errs).flatten())
                flat_params_errs.insert(0, index)

                for key, value in zip(columns.keys(), flat_params_errs):
                    columns[key].append(value)
            #to_save_df = pd.DataFrame(columns)
            #to_save_df.to_csv('test.csv')
        else:
            cycle_string = 'AllCycles'
            columns = {'Ch.': [], 'A': [], 'B': [], 'C': [], 'A_err':[], 'B_err':[], 'C_err':[]}

            for index, row in data.iterrows():
                x = row[required_columns_x].values
                y = row[required_columns_y].values ** 2

                coef, error = np.polyfit(x, y, 2, cov=True)
                fit_error = np.sqrt(np.diag(error))
                
                columns['Ch.'].append(index)
                columns['A'].append(coef[2])
                columns['B'].append(coef[1])
                columns['C'].append(coef[0])
                columns['A_err'].append(fit_error[2])
                columns['B_err'].append(fit_error[1])
                columns['C_err'].append(fit_error[0])

        to_save_df = pd.DataFrame(columns)
        coef_save_path = f"{output_directory}/COEFFICIENT-{cycle_string}-{date}.csv"
        to_save_df.to_csv(coef_save_path, index=False)

        if not all(col in data.columns for col in required_columns_x + required_columns_y):
            logging.info(f"Skipping {file_path} as it does not have the required columns.")
            return
        

    except Exception as e:
        logging.error(f"Error processing {file_path}. Error: \"{e}\"")


def sub_folder_name(fw_remove, output_directory):
    if fw_remove:
        fw_removed_str = '_'.join([f'FW{fw}' for fw in fw_remove]) + '_removed'
        subdir = f"{output_directory}/{fw_removed_str}"
        #os.makedirs(subdir, exist_ok=True)
        output_directory = os.path.abspath(subdir)
        return output_directory
    else:
        subdir = f"{output_directory}/ALL_FW"
        #os.makedirs(subdir, exist_ok=True)
        output_directory = os.path.abspath(subdir)
        return output_directory

def csv_file_name(file_path, fw_remove):
    date = os.path.basename(file_path).split('-')[1]
    if fw_remove:
        fw_removed_str = '_'.join([f'FW{fw}' for fw in fw_remove]) + '_removed'
        output_file = os.path.join(output_directory, f'COEFFICIENT_{date}_{fw_removed_str}.csv')
        return output_file
    else:
        output_file = os.path.join(output_directory, f'COEFFICIENT_ALL_FW_{date}.csv')
        return output_file


if __name__ == "__main__":
    log_directory = '../logs'
    os.makedirs(log_directory, exist_ok=True)
    log_filename = os.path.join(log_directory, f"{os.path.basename(__file__)}_{datetime.now().strftime('%Y_%m_%d_%H_%M_%S')}.log")
    logging.basicConfig(filename=log_filename, level=logging.INFO, format='%(asctime)s - %(message)s')

    #user args
    parser = argparse.ArgumentParser(description="Process CSV files to compute coefficients. Coefficients can be computed\
                                     for each cycle or for all cycles in the data.")
    parser.add_argument('-i', '--input_directory', type=str, default="../results/Data/Raw", help='Path to the directory containing input CSV files. Defaults to ../results/Data/Raw', metavar='')
    parser.add_argument('-o', '--output_directory', type=str, default="../results/Data/Coefficients/", help='Path where the resulting coefficient CSV files will be saved. Defaults to ../results/Data/Coefficients/', metavar='')
    parser.add_argument('-f', '--fw_remove', type=int, nargs='*', choices=range(1,7), help='Filter Wheel positions to remove from data before computing coefficients. Can specify multiple FW positions.')
    parser.add_argument('-c', '--cycle_flag', action= 'store_true',  help='Use -c to compute coefs for all cycles. To generate coefs for each cycle use this option: -c')
    args = parser.parse_args()

    # Check if input directory exists
    if not os.path.exists(args.input_directory):
        logging.error(f"Input directory '{args.input_directory}' not found!")
        raise FileNotFoundError(f"Input directory '{args.input_directory}' not found!")
    
    # Ensure output directory exists
    if not os.path.exists(args.output_directory):
        os.makedirs(args.output_directory)

    # Retrieve all .csv files in the directory
    csv_files = [os.path.join(args.input_directory, file) for file in os.listdir(args.input_directory) if file.endswith('.csv')]

    # Process each .csv file
    for file_path in csv_files:
        output_directory = sub_folder_name(args.fw_remove, args.output_directory)
        # Ensure output directory exists
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)
        logging.info(f"Processing CSV file: '{file_path}' ...")
        process_csv(file_path, output_directory, args.cycle_flag, args.fw_remove)
    logging.info("Processing completed!")