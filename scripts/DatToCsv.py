#!/usr/bin/env python3

import os
import pandas as pd
import re
import csv
import argparse
import logging
from datetime import datetime

class DatToCsvConverter:
    def __init__(self, file_path, year):
        self.file_path = file_path
        self.year = year
        self.days = {}
        self.column_names = self._generate_column_names()
        self.read_file()

    def read_file(self):
        with open(self.file_path, 'r') as file:
            all_lines = file.readlines()

        for index, line in enumerate(all_lines):
            line = line.strip()
            if line.startswith(self.year):
                data_of_a_day = all_lines[index: index + 1563] if index + 1563 + 1 < len(all_lines) else all_lines[index:len(all_lines)]
                self.days[line] = [l.strip() for l in data_of_a_day]

    def _generate_column_names(self):
        col_labels = ['Ch.','ave_ped','sig_ped','ave_raw','sig_raw','ave_sig','sig_sig']
        column_names = []

        for i in range(1, 11):
            for j in range(1, 7):
                for label in col_labels:
                    column_name = f'Cycle{i}_{j}_{label}'
                    column_names.append(column_name)

        return column_names

    def _custom_sort(self, key):
        try:
            parts = key.split('_')
            cycle_num = int(parts[0].replace('Cycle', ''))
            subkey_num = int(parts[1])
            return cycle_num, subkey_num
        except:
            return (0, 0)

    def process_day_data(self, day_data):
        day_data = [re.sub(r'\s+', ',', line) for line in day_data]
        day_data = [line.split(",") for line in day_data]
        
        cycle_fw = {}
        for index, line in enumerate(day_data):
            if line[0].startswith("Cycle"):
                current_cycle = int(line[1])
                current_fw = int(line[-1])
                fw_data = day_data[index + 2: index + 26]
                cycle_fw[f"Cycle-{current_cycle}-{current_fw}"] = fw_data

        flattened_data = []
        for i in range(24):
            row_data = []
            for key in sorted(cycle_fw.keys(), key=self._custom_sort):
                row_data.extend(cycle_fw[key][i])
            flattened_data.append(row_data)

        return flattened_data

    def write_to_csv(self, output_directory):
        for day, data in self.days.items():
            processed_data = self.process_day_data(data)
            output_file = os.path.join(output_directory, f'CSV-{day}-data.csv')
            with open(output_file, 'w', newline='') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(self.column_names)
                writer.writerows(processed_data)
            message = f"Processed {self.file_path} and saved as {output_file}"
            if args.verbose: print(message)
            logging.info(message)

if __name__ == '__main__':
    # Setup logging
    log_directory = '../logs'
    os.makedirs(log_directory, exist_ok=True)
    log_filename = os.path.join(log_directory, f"{os.path.basename(__file__)}_{datetime.now().strftime('%Y_%m_%d_%H_%M_%S')}.log")
    logging.basicConfig(filename=log_filename, level=logging.INFO, format='%(asctime)s - %(message)s')

    parser = argparse.ArgumentParser(description="Convert .dat files to CSV.")
    parser.add_argument('-i', '--input_directory', type=str, help='Path to the directory containing input .dat files.')
    parser.add_argument('-o','--output_directory', type=str, default=None, help='Optional path where the resulting CSV files will be saved. If not provided, files will be saved in ../results/Data/Raw.')
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Enable verbose mode to display messages in the terminal.')
    args = parser.parse_args()

    # If no output directory is provided by the user, set it to ../results/Data/Raw
    if args.output_directory is None:
        args.output_directory = "../results/Data/Raw"
        if not os.path.exists(args.output_directory):
            os.makedirs(args.output_directory)

    # Retrieve all .dat files in the directory
    dat_files = [os.path.join(args.input_directory, file) for file in os.listdir(args.input_directory) if file.endswith('.dat')]
    
    # Define valid years
    valid_years = ['2020', '2021', '2022', '2023']

    # Process each .dat file
    for file_path in dat_files:
        # Extract the year from the filename
        year = os.path.basename(file_path).split('_')[0]
        if year in valid_years:
            logging.info(f"\nProcessing .dat file: '{file_path}' ...")
            converter = DatToCsvConverter(file_path, year)
            converter.write_to_csv(args.output_directory)
    logging.info("\nProcessing completed!")

