#!/usr/bin/env python3

import argparse
import os
import pandas as pd
import re
import numpy as np

def get_coef_files(input_folder):
    csv_files = [os.path.join(root, name)
        for root, dirs, files in os.walk(input_folder)
        for name in files
        if name.endswith(".csv")]
                
    return csv_files

def gain_sub_folder(input_folder, output_folder):
    coef_sub_folders = [sub for sub in os.listdir(input_folder) if os.path.isdir(os.path.join(input_folder, sub))]
    [os.makedirs(os.path.join(output_folder, sub_folder), exist_ok=True) for sub_folder in coef_sub_folders]
    return


def all_cycle_gain(input_folder):
    csv_files = get_coef_files(input_folder)
    all_cycle_files = [files for files in csv_files if 'AllCycles' in files]
    return all_cycle_files

def cycle_wise_gain(input_folder):
    csv_files = get_coef_files(input_folder)
    cycle_wise_files = [files for files in csv_files if 'CycleWise' in files]
    return cycle_wise_files

def get_gain_all_cycle(input_folder, output_folder):
    csv_files = all_cycle_gain(input_folder)
    for each_csv in csv_files:
        sub_folder = each_csv.split("/")[-2]
        date = each_csv.split("/")[-1].split(".")[0].split("-")[-1]
        gain_save_directory = f"{output_folder}/{sub_folder}"
        df = pd.read_csv(each_csv)
        save_df = pd.DataFrame()
        
        gain = (df['B'] * 1_000_000)/(2.08)
        gain_err = (df['B_err'] * 1_000_000)/(2.08)

        save_df['Gain'] = gain
        save_df['Gain_err'] = gain_err
        
        save_df.to_csv(f"{gain_save_directory}/Gain-AllCycles-{date}.csv", index=False)
    return
def cycle_average_stats(input_folder, output_folder):
    #I decided to modify Coefficient csv files here. It is better to not touch
    #the step where I convert Raw to Coefficients.
    csv_files = cycle_wise_gain(input_folder)
    for each_csv in csv_files:
        coef_file = pd.read_csv(each_csv)
        coef_B = [col for col in coef_file.columns if re.compile(r'Cycle\d+_[B]$').match(col)]
        coef_B_err = [col for col in coef_file.columns if re.compile(r'Cycle\d+_[B]_err$').match(col)]

        weight = [1/(coef_file[cols] **2) for cols in coef_B_err]
        weight_df = pd.concat(weight, axis=1, ignore_index=True)
        weight_df.columns = coef_B
        weighted_B = coef_file[coef_B].multiply(weight_df)
        weighted_sum = weighted_B.sum(axis=1)
        sum_of_weights = weight_df.sum(axis=1)
        

        #print(weighted_B)
        #below equivalent method to find weights
        #weight = []
        #for cols in coef_B_err:
            #w = 1/(coef_file[cols] **2)
            #weight.append(w)
        #print(weight)'''
        #above equivalent method to find weights
        #coef_file.to_csv(each_csv, index=False)
        #below original line before changing column name
        #coef_file['B_weighted_avg'] = weighted_sum / sum_of_weights
        coef_file['B'] = weighted_sum / sum_of_weights
        #below original line before changing columns name
        #coef_file['B_weighted_err'] = (1/(len(coef_B)-1)) * (1/(weighted_sum))
        coef_file['B_err'] = np.sqrt((1/(len(coef_B)-1)) * (1/(sum_of_weights)))

        coef_file.to_csv(each_csv, index=False)
    return


def get_gain_cycle_wise(input_folder, output_folder):
    csv_files = cycle_wise_gain(input_folder)
    for each_csv in csv_files:
        sub_folder = each_csv.split("/")[-2]
        date = each_csv.split("/")[-1].split(".")[0].split("-")[-1]
        gain_save_directory = f"{output_folder}/{sub_folder}"
        full_file_path = f"{gain_save_directory}"
        #print(full_file_path)
        df = pd.read_csv(each_csv)
        save_df = pd.DataFrame()
        coef_B = [col for col in df.columns if re.compile(r'Cycle\d+_[B]$').match(col)]
        coef_B_err = [col for col in df.columns if re.compile(r'Cycle\d+_[B]_err$').match(col)]
        for cycle_B in coef_B:
            save_df[cycle_B] = (df[cycle_B] * 1_000_000)/(2.08)
        for cycle_B_err in coef_B_err:
            save_df[cycle_B_err] = (df[cycle_B_err] * 1_000_000)/(2.08)
        
        #original lines before changing column names
        #save_df['B_avg'] = (df['B_weighted_avg'] * 1_000_000)/(2.08)
        #save_df['B_weighted_err'] = (df['B_weighted_err'] * 1_000_000)/(2.08)

        save_df['Gain'] = (df['B'] * 1_000_000)/(2.08)
        save_df['Gain_err'] = (df['B_err'] * 1_000_000)/(2.08)
        #original line before changing column names
        #save_df.columns = [col.replace('B', 'Gain') for col in save_df.columns if 'B' in col ]
        save_df.to_csv(f"{gain_save_directory}/Gain-CycleWise-{date}.csv", index=False)


parser = argparse.ArgumentParser(description="Process coefficient CSV files to compute gain values.")
parser.add_argument('-i','--input_directory', type=str, default="../results/Data/Coefficients", help='Path to the directory containing coefficient CSV files. Defaults to ../results/Data/Coefficients.')
parser.add_argument('-o','--output_directory', type=str, default="../results/Data/Gain", help='Optional path where the resulting Gain CSV files will be saved. Defaults to ../results/Data/Gain.')
args = parser.parse_args()

gain_sub_folder(args.input_directory, args.output_directory)
cycle_average_stats(args.input_directory, args.output_directory)
get_gain_all_cycle(args.input_directory, args.output_directory)
get_gain_cycle_wise(args.input_directory, args.output_directory)