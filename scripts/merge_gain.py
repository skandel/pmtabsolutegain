#!/usr/bin/env python3

import os
import pandas as pd
import datetime
import argparse

def get_gain_files(input_folder):
    csv_files = [os.path.join(root, name)
        for root, dirs, files in os.walk(input_folder)
        for name in files
        if name.endswith(".csv")]
    return csv_files


def merge_sub_folder(input_folder, output_folder):
    coef_sub_folders = os.listdir(input_folder)
    [os.makedirs(os.path.join(output_folder, sub_folder), exist_ok=True) for sub_folder in coef_sub_folders]
    return


def sort_date(filename):
    date = filename.split('/')[-1].split('-')[2].split(".")[0]
    return datetime.datetime.strptime(date, '%Y_%m_%d')


def get_fw_info(input_folder):
    csv_files = get_gain_files(input_folder)
    fw_tag = {fname.split("/")[-2] for fname in csv_files}
    cycle_tag = ['CycleWise', 'AllCycles']    
    separator = {f"{fw}_{cycle}": [] for fw in fw_tag for cycle in cycle_tag}

    return separator

    

def collect_files_in_dict(input_folder):
    separator = get_fw_info(input_folder)
    csv_files = get_gain_files(input_folder)
    fw = csv_files[0].split("/")[-2]
    cy = csv_files[0].split("-")[-2]
    for files in csv_files:
        fw_tag = files.split("/")[-2]
        cycle_tag = files.split("-")[-2]
        for category in separator.keys():
                if fw_tag in category and cycle_tag in category:
                     separator[category].append(files)
    return separator

#x = collect_files_in_dict('../results/Data/Gain')


def make_time_series(input_folder, output_folder):
    files_dict = collect_files_in_dict(input_folder)
    #Check if the dict has files for categories
    for category, file_list in files_dict.items():
        if len(file_list) == 0:
            print(f'You do not have Gain files for {category}. Skipping. . . ')
        else:
            files_dict[category] = sorted(file_list, key=sort_date)
            print(f"Merging {category}")
            merge_file_name = f"{category}.csv"
            dates = [filename.split('/')[-1].split('-')[2].split(".")[0] for filename in files_dict[category]]
            gain_col = [f'Gain-{day}' for day in dates]
            gain_err_col = [f'GainErr-{day}' for day in dates]
            merge_df = pd.DataFrame(columns=gain_col + gain_err_col)

            for gain_files in file_list:
                gain_df = pd.read_csv(gain_files)
                for cols in merge_df.columns:
                    date_in_col = cols.split("-")[-1]
                    if ("Err" in cols) and  (date_in_col in gain_files):
                        merge_df[cols] = gain_df["Gain_err"]
                    elif ("Err" not in cols) and (date_in_col in gain_files):
                        merge_df[cols] = gain_df["Gain"]
            merge_df.to_csv(f"{output_folder}/MERGED-{category}.csv")

    return 


#parsing
parser = argparse.ArgumentParser(description="Process coefficient CSV files to compute gain values.")
parser.add_argument('-i','--input_directory', type=str, default="../results/Data/Gain", help='Path to the directory containing individual gain files. Defaults to ../results/Data/Gain.')
parser.add_argument('-o','--output_directory', type=str, default="../results/Data/Gain", help='Optional path where the resulting merged Gain CSV files will be saved. Defaults to ../results/Data/Gain.')
args = parser.parse_args()

os.makedirs(os.path.join(args.output_directory, "MERGED"), exist_ok=True)

make_time_series(args.input_directory, f"{args.output_directory}/MERGED")

